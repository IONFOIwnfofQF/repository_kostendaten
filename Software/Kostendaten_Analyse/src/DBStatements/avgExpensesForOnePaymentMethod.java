package DBStatements;

public class avgExpensesForOnePaymentMethod {

	private Double avgExpense;
	private int paymentmethod;
	
	public Double getAvgExpense() {
		return avgExpense;
	}
	public void setAvgExpense(Double avgExpense) {
		this.avgExpense = avgExpense;
	}
	public int getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(int paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	
	public avgExpensesForOnePaymentMethod(){}
	
	public avgExpensesForOnePaymentMethod(Double avgExpense, int paymentmethod) {
		this.avgExpense = avgExpense;
		this.paymentmethod = paymentmethod;
	}
	@Override
	public String toString() {
		return "avgExpensesForOnePaymentMethod [avgExpense=" + avgExpense
				+ ", paymentmethod=" + paymentmethod + "]";
	}
	
	
	
	
}
