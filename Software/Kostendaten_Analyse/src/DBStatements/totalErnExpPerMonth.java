package DBStatements;

public class totalErnExpPerMonth {
	
	private boolean isexpense;
	private double totalamount;
	private String month;
	
	
	public boolean getIsExpense() {
		return isexpense;
	}
	public void setIsExpense(boolean isexpense) {
		this.isexpense = isexpense;
	}
	public double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	
	public totalErnExpPerMonth(){}
	public totalErnExpPerMonth(boolean isexpense, double totalamount, String month) {
		super();
		this.isexpense = isexpense;
		this.totalamount = totalamount;
		this.month = month;
	}
	
	@Override
	public String toString() {
		return "totalErnExpPerMonthPerPm [isexpense=" + isexpense
				+ ", totalamount=" + totalamount + ", month=" + month + "]";
	}
	
	
	
	
}
