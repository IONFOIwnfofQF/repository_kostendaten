package DBStatements;

public class TotalExpensesPerTransactionpurposePerMonth {

	private String transactionpurpose;
	private double totalAmount;
	
	public String getTransactionpurpose() {
		return transactionpurpose;
	}
	public void setTransactionpurpose(String transactionpurpose) {
		this.transactionpurpose = transactionpurpose;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public TotalExpensesPerTransactionpurposePerMonth(){}
	
	public TotalExpensesPerTransactionpurposePerMonth(
			String transactionpurpose, double totalAmount) {
		super();
		this.transactionpurpose = transactionpurpose;
		this.totalAmount = totalAmount;
	}
	
	@Override
	public String toString() {
		return "TotalExpensesPerTransactionpurposePerMonth [transactionpurpose="
				+ transactionpurpose + ", totalAmount=" + totalAmount + "]";
	}
	
	
	
	
	
}
