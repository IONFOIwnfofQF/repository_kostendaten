package DBStatements;

public class TotalExpensesForOneMonthForOnePaymentmethod {

	private double sumAmount;
	private String paymentmethod;
	private String month;	
	
	public double getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(double sumAmount) {
		this.sumAmount = sumAmount;
	}
	public String getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String _month) {
		month = _month;
	}
	
	public TotalExpensesForOneMonthForOnePaymentmethod(){}
	
	public TotalExpensesForOneMonthForOnePaymentmethod(double _sumAmount,
			String _paymentmethod, String _month) {
		this.sumAmount = _sumAmount;
		this.paymentmethod = _paymentmethod;
		this.month = _month;
	}
	@Override
	public String toString() {
		return "TotalExpensesForOneMonthForOnePaymentmethod [sumAmount="
				+ sumAmount + ", paymentmethod=" + paymentmethod + ", month="
				+ month + "]";
	}
	
	
	
	
}
