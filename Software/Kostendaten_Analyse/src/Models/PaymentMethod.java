package Models;

public class PaymentMethod {

	private int paymentmethodId;
	private String paymentmethod;

	public String getPaymentmethod() {
		return paymentmethod;
	}

	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}

	
	public int getPaymentmethodId() {
		return paymentmethodId;
	}

	public void setPaymentmethodId(int paymentmethodId) {
		this.paymentmethodId = paymentmethodId;
	}

	public PaymentMethod(){
		
	}
	
	public PaymentMethod(String paymentmethod) {

		this.paymentmethod = paymentmethod;
	}
	
	public PaymentMethod(int paymentmethodId, String paymentmethod) {

		this.paymentmethodId = paymentmethodId;
		this.paymentmethod = paymentmethod;
	}

	@Override
	public String toString() {
		return "PaymentMethod [paymentmethodId=" + paymentmethodId
				+ ", paymentmethod=" + paymentmethod + "]";
	}
	
	
	
}
