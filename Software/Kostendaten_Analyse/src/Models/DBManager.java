package Models;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import DBStatements.*;
import au.com.bytecode.opencsv.CSVReader;

import com.mysql.fabric.xmlrpc.base.Data;
import com.mysql.jdbc.PreparedStatement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;


public class DBManager {
	
	private static String dbuser = "root";
	private static String dbpwd = "password";
	private static String database = "database_kostendaten";
	private static String dburl = "jdbc:mysql://localhost:3306/";
	private static String driverName = "com.mysql.jdbc.Driver";	
	private static Connection c;
	
	public DBManager(Connection con){
		c = con;
	}
	
	public static Connection getConnection() throws ClassNotFoundException{
		try{
			Class.forName(driverName);
			c = DriverManager.getConnection(dburl+database, dbuser, dbpwd);
			
		}catch(SQLException ex){
			ex.printStackTrace();
		}
		
		return c;
		
	}
	
	public static void closeConnection(Connection c) throws SQLException{
		if(c != null){
			c.close();
		}
	}

	
	public static double GetAvgAmountFromDBForOnePaymentMethod(int userId, int paymentmethod, int monthsback) throws SQLException, ClassNotFoundException{
		String query = "SELECT AVG(totalPerMonth) FROM (SELECT SUM(amount) as totalPerMonth,"
				+ " paymentmethod, Month(date) FROM transaction WHERE isexpense = true"
				+ " AND paymentmethod = "
				+ paymentmethod
				+ " AND user = "
				+ userId
				+ " AND date BETWEEN DATE_SUB(now(), INTERVAL "
				+ monthsback
				+ " MONTH) AND now() GROUP BY Month(date)) AS totalAvg";
		double amount =0;		
		
		try {
			getConnection();
			Statement stmt = c.createStatement();				

			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()){
				amount = rs.getDouble("AVG(totalPerMonth)");
				
			}
			stmt.close();
			closeConnection(c);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return amount;
			
	}

	public static ArrayList<PaymentMethod> GetPaymentMethods(){
		
		String sql = "SELECT * FROM paymentmethod";
		ArrayList<PaymentMethod> paymentmethods = new ArrayList<PaymentMethod>();
		try {
			getConnection();
			Statement stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				PaymentMethod pm = new PaymentMethod();
				
				pm.setPaymentmethodId(rs.getInt("paymentmethodId"));
				pm.setPaymentmethod(rs.getString("paymentmethod"));
				
				paymentmethods.add(pm);
			}
			
			stmt.close();
			closeConnection(c);
			
			return paymentmethods;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static ArrayList<TransactionPurpose> GetAllTransactiontypesForExpenses(int userId){
		
		String sql = "SELECT * FROM transactionpurpose WHERE user='";
		sql += userId;
		sql += "'";
		ArrayList<TransactionPurpose> purposes = new ArrayList<TransactionPurpose>();
		
		try{
			
			getConnection();
			Statement stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				TransactionPurpose tp = new TransactionPurpose();
				
				tp.setIsexpense(true);
				tp.setTransactionpurpose(rs.getString("transactionpurpose"));
				tp.setUser(userId);
				tp.setTransactionpurposeid(rs.getInt("transactionpurposeId"));
				
				purposes.add(tp);
			}
			
			stmt.close();			
			closeConnection(c);
			
			return purposes;
			
		}catch(ClassNotFoundException | SQLException e){
			e.printStackTrace();
			return null;
		}
				
		
	}
	
	public static ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> GetAllExpensesByNMonthsForOnePaymentMethod(int userid, int paymentmethod, int monthsback){
		
		String sql ="SELECT SUM(amount), paymentmethod, Month(date) FROM transaction WHERE isexpense = true AND paymentmethod = "
				+ paymentmethod +" AND user = "
				+ userid + " AND date BETWEEN DATE_SUB(now(), INTERVAL "
				+ monthsback + " MONTH) AND now() GROUP BY Month(date)";
		
		ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> list = new ArrayList<TotalExpensesForOneMonthForOnePaymentmethod>();
		
		Statement stmt = null;
		
	
			try {
				getConnection();
				stmt = c.createStatement();
				
				ResultSet rs = stmt.executeQuery(sql);
				
				while(rs.next()){
					TotalExpensesForOneMonthForOnePaymentmethod exp = new TotalExpensesForOneMonthForOnePaymentmethod();
					
					exp.setMonth(ConvertIntMonthToString(rs.getInt("Month(date)")));
					exp.setPaymentmethod(GetPaymentMethodWithPaymentMethodId(rs.getInt("paymentmethod")));
					exp.setSumAmount(rs.getDouble("SUM(amount)"));
					
					list.add(exp);
					
				}
					
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
	}
	
	public static ArrayList<avgExpensesForOnePaymentMethod> getAvgExpensesForAllPaymentTypesForNMonths(int userId, int monthsback){
		String sql = "SELECT AVG(totalPerMonth), pm FROM"
				+ " (SELECT SUM(amount) as totalPerMonth, "
				+ "paymentmethod as pm, Month(date) "
				+ "FROM transaction WHERE isexpense "
				+ "= true AND user = "
				+ userId
				+ " AND date BETWEEN DATE_SUB(now(), INTERVAL "
				+ monthsback
				+ " MONTH) AND now() GROUP BY paymentmethod,"
				+ " Month(date))  AS totalAvg GROUP BY pm";
		
		Statement stmt = null;
		
		ArrayList<avgExpensesForOnePaymentMethod> avgExpForAllPms = new ArrayList<avgExpensesForOnePaymentMethod>();
		
		try{
			getConnection();
			
			stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				avgExpensesForOnePaymentMethod avgexp = new avgExpensesForOnePaymentMethod();
				
				avgexp.setAvgExpense(rs.getDouble("AVG(totalPerMonth)"));
				avgexp.setPaymentmethod(rs.getInt("pm"));
				
				avgExpForAllPms.add(avgexp);
			}
			
		}catch(SQLException | ClassNotFoundException ex){}
		
		return avgExpForAllPms;
		
	}
	
	
	public static double getAvgEarningOrExpenseForNMonths(int userId, int monthsback, boolean isExpense){
		double avg = 0.0;
		PreparedStatement stmt = null;
		String sql = "SELECT AVG(totalPerMonth) FROM(SELECT SUM(amount) as totalPerMonth, "
				+ "Month(date) FROM transaction WHERE isexpense = ? AND user = ? "
				+ "AND date BETWEEN DATE_SUB(now(), INTERVAL ? MONTH) AND now() "
				+ "GROUP BY Month(date))  AS totalAvg ";
		
		try{
			getConnection();
			
			stmt = (PreparedStatement) c.prepareStatement(sql);
			stmt.setBoolean(1, isExpense);
			stmt.setInt(2, userId);
			stmt.setInt(3, monthsback);
			
			ResultSet rs = stmt.executeQuery();
			
			rs.next();
			
			avg = rs.getDouble("AVG(totalPerMonth)");
					
			closeConnection(c);
			
		}catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return avg;
		
	}
	
	public static ArrayList<totalErnExpPerMonth> getEarningsAndExpensesForNMonths(int userId, int monthsback){
		
		ArrayList<totalErnExpPerMonth> list = new ArrayList<totalErnExpPerMonth>();
		
		Statement stmt = null;
		
		String sql = "SELECT isexpense, SUM(amount), Month(date) FROM "
				+ "transaction WHERE user="
				+ userId
				+ " AND date BETWEEN DATE_SUB(now(), INTERVAL "
				+ monthsback
				+ " MONTH) AND now() GROUP BY isexpense, Month(date) ";
				
		try {
			getConnection();
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				totalErnExpPerMonth totalErnExp = new totalErnExpPerMonth();
				totalErnExp.setIsExpense(rs.getBoolean("isexpense") );
				totalErnExp.setTotalamount(rs.getDouble("SUM(amount)"));
				totalErnExp.setMonth(ConvertIntMonthToString(rs.getInt("Month(date)")));
				
				list.add(totalErnExp);
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	public static ArrayList<TotalExpensesPerTransactionpurposePerMonth> getTotalExpensesForEachTransactionTypeWithinNMonths(int userId, int monthsback) throws Exception{
		
		ArrayList<TotalExpensesPerTransactionpurposePerMonth> list = new ArrayList<TotalExpensesPerTransactionpurposePerMonth>();
		PreparedStatement stmt = null;
		String sql = "SELECT SUM(amount), transactionpurpose FROM transaction WHERE user=? AND isexpense=1 AND date BETWEEN DATE_SUB(now(), INTERVAL ? MONTH) AND now() GROUP BY transactionpurpose";
		
		try{
			getConnection();
			stmt = (PreparedStatement) c.prepareStatement(sql);
			stmt.setInt(1, userId);
			stmt.setInt(2, monthsback);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				TotalExpensesPerTransactionpurposePerMonth exp = new TotalExpensesPerTransactionpurposePerMonth();
				exp.setTotalAmount(rs.getDouble("SUM(amount)"));
				exp.setTransactionpurpose(GetTransactionPurposeWithId(rs.getInt("transactionpurpose")));
							
				list.add(exp);
				
			}

			stmt.close();
			closeConnection(c);		

			return list;
			
		}catch(SQLException | ClassNotFoundException ex){throw ex;}
		
	}
	
	
	
	//creating all of the tables
	
 	public static void CreateTableUser(){
		java.sql.PreparedStatement stmt = null;
		String sql = "CREATE TABLE IF NOT EXISTS `user` (`userId` int(11) NOT NULL AUTO_INCREMENT,`username` varchar(20) NOT NULL,PRIMARY KEY (`userId`))";
		try {
			getConnection();
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();			
			closeConnection(c);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void CreateTablePaymentmethod(){
		java.sql.PreparedStatement stmt = null;
		String sql = "CREATE TABLE `paymentmethod` ("
				+"`paymentmethodId` int(11) NOT NULL AUTO_INCREMENT,"
				+"`paymentmethod` varchar(20) NOT NULL,"
				+"PRIMARY KEY (`paymentmethodId`)"
				+")";
		try {
			getConnection();
			stmt = (PreparedStatement) c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();			
			closeConnection(c);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void CreateTableTransactionPurpose(){
		java.sql.PreparedStatement stmt = null;
		String sql = "CREATE TABLE IF NOT EXISTS `transactionpurpose` ("
					+"`transactionpurposeId` int(11) NOT NULL AUTO_INCREMENT,"
					+"`isexpense` tinyint(1) NOT NULL,"
					+"`transactionpurpose` varchar(45) NOT NULL,"
					+"`user` int(11) NOT NULL,"
					+"PRIMARY KEY (`transactionpurposeId`),"
					+"FOREIGN KEY (`user`) REFERENCES `user` (`userId`)"
					+") ";
		
		try {
			getConnection();
			stmt = (PreparedStatement) c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();			
			closeConnection(c);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void CreateTableTransaction(){
		java.sql.PreparedStatement stmt = null;
		String sql = "CREATE TABLE IF NOT EXISTS `transaction` (`transactionId` int(11) NOT NULL AUTO_INCREMENT,`amount` decimal(11,2) NOT NULL,`isexpense` tinyint(1) NOT NULL,`date` date NOT NULL,`user` int(11) NOT NULL,`paymentmethod` int(11) NOT NULL,`transactionpurpose` int(11) NOT NULL,`description` varchar(255) NOT NULL,PRIMARY KEY (`transactionId`),FOREIGN KEY (`paymentmethod`) REFERENCES `paymentmethod` (`paymentmethodId`),FOREIGN KEY (`transactionpurpose`) REFERENCES `transactionpurpose` (`transactionpurposeId`),FOREIGN KEY (`user`) REFERENCES `user` (`userId`)) ";
		
		try {
			getConnection();
			stmt = (PreparedStatement) c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();			
			closeConnection(c);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Inserting Data into DB
	
	public static void AddUser(User u){
		String sql = "INSERT INTO USER(username) VALUES (?)";
		java.sql.PreparedStatement stmt = null;
		try {
			getConnection();
			stmt = c.prepareStatement(sql);
			stmt.setString(1, u.getUsername());
			stmt.executeUpdate();
			stmt.close();
			closeConnection(c);
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void AddTransactionPurpose(TransactionPurpose tp){
		java.sql.PreparedStatement stmt = null;
		try {
			getConnection();
			String sql = "INSERT INTO transactionpurpose(isexpense, transactionpurpose, user) VALUES(?,?,?)";
			stmt = c.prepareStatement(sql);
			
			stmt.setBoolean(1, tp.getIsexpense());
			stmt.setString(2, tp.getTransactionpurpose());
			stmt.setInt(3, tp.getUser());
			
			stmt.executeUpdate();
			stmt.close();
			closeConnection(c);
			
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void AddPayment(PaymentMethod paymentmethod){
		java.sql.PreparedStatement stmt = null;
		try {
			getConnection();
			String sql = "INSERT INTO paymentmethod(paymentmethod) VALUES(?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1,paymentmethod.getPaymentmethod());
			
			stmt.executeUpdate();
			stmt.close();
			closeConnection(c);
			
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void AddTransaction(Transaction t){
		java.sql.PreparedStatement stmt = null;
		try{
			getConnection();
			String sql = "INSERT INTO transaction(`amount`, `isexpense`,`date`,`user`,`paymentmethod`,`transactionpurpose`, `description`) VALUES(?,?,?,?,?,?,?)";
			
			stmt = c.prepareStatement(sql);
			
			stmt.setDouble(1, t.getAmount());
			stmt.setBoolean(2, t.getIsexpense());
			java.sql.Date d = java.sql.Date.valueOf(t.getDate());
			stmt.setDate(3, d);
			stmt.setInt(4, t.getUser());
			stmt.setInt(5, t.getPaymentmethod());
			stmt.setInt(6, t.getTransactionpurpose());
			stmt.setString(7, t.getDescription());
			
			
			stmt.executeUpdate();
			
			stmt.close();	
			closeConnection(c);
			
		}catch(ClassNotFoundException | SQLException e){
			e.printStackTrace();
		}
		
	}
	
	public static String GetPaymentMethodWithPaymentMethodId(int pmId){
		Statement stmt = null;
		
		String paymentmethod ="";
		String sql ="SELECT paymentmethod from paymentmethod where paymentmethodId = ";
		sql += pmId;
		
		try {
			getConnection();
			
			stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			rs.next();
			
			paymentmethod = rs.getString("paymentmethod");
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return paymentmethod;
		
	}
	
	public static String GetTransactionPurposeWithId(int tId){
		Statement stmt = null;
		
		String transactionpurpose ="";
		String sql ="SELECT transactionpurpose FROM transactionpurpose WHERE transactionpurposeId = ";
		sql += tId;
		
		try {
			getConnection();
			
			stmt = c.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			rs.next();
			
			transactionpurpose = rs.getString("transactionpurpose");
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return transactionpurpose;
		
	}
	
	
	public static String ConvertIntMonthToString(int month){
		
		ArrayList<String> months = new ArrayList<String>();
		months.add("Jan");
		months.add("Feb");
		months.add("Mar");
		months.add("Ap");
		months.add("May");
		months.add("Ju");
		months.add("Jul");
		months.add("Aug");
		months.add("Sept");
		months.add("Oct");
		months.add("Nov");
		months.add("Dec");

		return months.get(month - 1);
	}
	 
}
	
	

