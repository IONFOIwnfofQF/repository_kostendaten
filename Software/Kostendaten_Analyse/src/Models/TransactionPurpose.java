package Models;

public class TransactionPurpose {

	private int transactionpurposeId;
	private Boolean isexpense;
	private String transactionpurpose;
	private int user;
	
	public Boolean getIsexpense() {
		return isexpense;
	}
	public void setIsexpense(Boolean isexpense) {
		this.isexpense = isexpense;
	}
	public String getTransactionpurpose() {
		return transactionpurpose;
	}
	public void setTransactionpurpose(String transactionpurpose) {
		this.transactionpurpose = transactionpurpose;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	
	public int getTransactionpurposeid() {
		return transactionpurposeId;
	}
	public void setTransactionpurposeid(int transactionpurposeid) {
		this.transactionpurposeId = transactionpurposeid;
	}
	
	
	public TransactionPurpose(){}
	
	public TransactionPurpose(int transactionpurposeid, Boolean isexpense,
			String transactionpurpose, int user){
		this.transactionpurposeId = transactionpurposeid;
		this.isexpense = isexpense;
		this.transactionpurpose = transactionpurpose;
		this.user = user;
	}
	
	public TransactionPurpose(Boolean isexpense,
			String transactionpurpose, int user) {

		this.isexpense = isexpense;
		this.transactionpurpose = transactionpurpose;
		this.user = user;
	}
	
	@Override
	public String toString() {
		return "TransactionPurpose [isexpense=" + isexpense
				+ ", transactionpurpose=" + transactionpurpose + ", user="
				+ user + "]";
	}
	
	
}
