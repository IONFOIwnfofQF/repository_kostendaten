package Models;

import java.time.LocalDate;

public class Transaction {
	
	private double amount;
	private Boolean isexpense;
	private LocalDate date;
	private int user;
	private int paymentmethod;
	private int transactionpurpose;
	private String description;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Boolean getIsexpense() {
		return isexpense;
	}
	public void setIsexpense(Boolean isexpense) {
		this.isexpense = isexpense;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getPaymentmethod() {
		return paymentmethod;
	}
	public void setPaymentmethod(int paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public int getTransactionpurpose() {
		return transactionpurpose;
	}
	public void setTransactionpurpose(int transactionpurpose) {
		this.transactionpurpose = transactionpurpose;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Transaction(){}
	
	public Transaction(double amount, Boolean isexpense, LocalDate date,
			int user, int paymentmethod, int transactionpurpose,
			String description) {
		super();
		this.amount = amount;
		this.isexpense = isexpense;
		this.date = date;
		this.user = user;
		this.paymentmethod = paymentmethod;
		this.transactionpurpose = transactionpurpose;
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", isexpense=" + isexpense
				+ ", date=" + date + ", user=" + user + ", paymentmethod="
				+ paymentmethod + ", transactionpurpose=" + transactionpurpose
				+ ", description=" + description + "]";
	}
	
	
	
	
	
	
	
}
