package Models;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

import DBStatements.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.chart.ChartUtilities;

public class Main {

	public static void main(String[] args) throws Exception {
	
		Connection c = null;
		DBManager manager = new DBManager(c);
		
		User david = new User(1, "David Retter-Rukundo");	
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Options: ");
		System.out.println("c ... create Tables and fetch Data from .csv into the Database");
		System.out.println("1 ... Visualize the Expenses for one Paymentmethod within a certain period");
		System.out.println("2 ... Visualize the Expenses for each Paymentmethod within a certain period");
		System.out.println("3 ... Visualize the Expenses and Earnings for each Month within a certain period");
		System.out.println("4 ... Visualize the total Expenses per Expensetype within a certain period");
		System.out.println("Please enter the letter: ");
		
		String s = sc.next();
		
		switch(s){
			case "c":
				System.out.println("Creating tables: ");
				createTables(manager);
				System.out.println("Fetching Data: ");
				InsertDataFromCsvIntoDB(manager);
				break;
			case "t":
				System.out.println("Avg Expenses: " + manager.getAvgEarningOrExpenseForNMonths(1, 12, true));
				System.out.println("Avg Earnings: " + manager.getAvgEarningOrExpenseForNMonths(1, 12, false));
				break;
			case "1":
				Scanner scanneruserId = new Scanner(System.in);
				System.out.println("Please enter your userId: ");	
				int id = Integer.parseInt(scanneruserId.next());
				
				Scanner scannerpmthd = new Scanner(System.in);
				System.out.println("Please enter the paymentmethod: ");	
				int mthd = Integer.parseInt(scannerpmthd.next());
				
				Scanner scannermnthsback = new Scanner(System.in);
				System.out.println("Please enter the number of months that you want to go back: ");	
				int month = Integer.parseInt(scannermnthsback.next());
				
				VisualizeTotalExpensesForNMonthsBackForOnePaymentMethod(manager, id, mthd, month);
				VisualizeTotalExpensesForNMonthsBackForOnePaymentMethodRelativeToAvg(manager, id, mthd, month);
				break;
			case "2":
				Scanner scanneruserId2 = new Scanner(System.in);
				System.out.println("Please enter your userId: ");	
				int id2 = Integer.parseInt(scanneruserId2.next());
				
				Scanner scannermnthsback2 = new Scanner(System.in);
				System.out.println("Please enter the number of months that you want to go back: ");	
				int month2 = Integer.parseInt(scannermnthsback2.next());
				
				VisualizeTotalExpensesForNMonthsBackForEachPaymentMethod(manager, id2, month2);
				VisualizeTotalExpensesForNMonthsBackForEachPaymentMethodRelativeToAVG(manager, id2, month2);
				break;
			case "3":
				Scanner scanneruserId3 = new Scanner(System.in);
				System.out.println("Please enter your userId: ");	
				int id3 = Integer.parseInt(scanneruserId3.next());
				
				Scanner scannermnthsback3 = new Scanner(System.in);
				System.out.println("Please enter the number of months that you want to go back: ");	
				int month3 = Integer.parseInt(scannermnthsback3.next());
				
				VisualizeTotalExpensesAndTotalEarningsForNMonthsBack(manager, id3, month3);		
				break;
			case "4":
				Scanner scanneruserId4 = new Scanner(System.in);
				System.out.println("Please enter your userId: ");	
				int id4 = Integer.parseInt(scanneruserId4.next());
				
				Scanner scannermnthsback4 = new Scanner(System.in);
				System.out.println("Please enter the number of months that you want to go back: ");	
				int month4 = Integer.parseInt(scannermnthsback4.next());
				
				VisualizeTotalExpensesPerTransactionPurposeForNMonthsBack(manager, id4, month4);		
				break;
		}

	}

	public static void createTables(DBManager manager){
		manager.CreateTableUser();	
		manager.CreateTablePaymentmethod();	
		manager.CreateTableTransactionPurpose();
		manager.CreateTableTransaction();
		System.out.println("Tables created");
	}
	
	public static void InsertDataFromCsvIntoDB(DBManager manager) throws IOException, ParseException{
		
		String fpath = "doc/Kosten.csv";	
		ReadFile reader = new ReadFile();		
		
		// creating a user:
		User david = new User(1, "David Retter-Rukundo");	
		manager.AddUser(david);
			
//		Getting all of the Payment Methods from the .csv and putting it into the DB
		PaymentMethod pm1 = new PaymentMethod("Bar");
		PaymentMethod pm2 = new PaymentMethod("Debitkarte");
		
		ArrayList<PaymentMethod> paymentmethods = new ArrayList<PaymentMethod>();
		paymentmethods.add(pm1);
		paymentmethods.add(pm2);
		
		for(int i = 0; i < paymentmethods.size(); i++){
			manager.AddPayment(paymentmethods.get(i));
		}
					
//		Getting all of the TransactionPurposes from the .csv and putting it into the DB
		ArrayList<TransactionPurpose> transactionpurposes = reader.getAllTransactionPurposesFromCSV(fpath);
		
		for(int i = 0; i < transactionpurposes.size(); i++){
			manager.AddTransactionPurpose(transactionpurposes.get(i));	
		}
		
		//Getting all of the Transactions from the .csv and putting it into the DB
		ArrayList<Transaction> transactions = reader.getAllTransactionsFromCSV(fpath, transactionpurposes);

		for(int i = 0; i < transactions.size(); i++){
			manager.AddTransaction(transactions.get(i));
		}
		
	}

	public static void VisualizeTotalExpensesForNMonthsBackForOnePaymentMethod(DBManager manager, int userId, int paymentMethod, int monthsback) throws IOException, ClassNotFoundException, SQLException{
		
		ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> listexp = manager.GetAllExpensesByNMonthsForOnePaymentMethod(userId, paymentMethod, monthsback);
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(int i = 0; i < listexp.size(); i++){
			TotalExpensesForOneMonthForOnePaymentmethod exp = listexp.get(i);
			dataset.addValue(exp.getSumAmount(), exp.getPaymentmethod(), exp.getMonth());		
		}
		
		JFreeChart barChart = ChartFactory.createBarChart("Total Expenses per Month per Paymentmethod", "Total Expenses per Month per Paymentmethods", "Expenses [�]", dataset,PlotOrientation.VERTICAL, true, true, false);
				
		//For Adding the line
		Double position = manager.GetAvgAmountFromDBForOnePaymentMethod(userId, paymentMethod, monthsback);
	
		ValueMarker marker = new ValueMarker(position);
		marker.setPaint(Color.yellow);
		marker.setLabel("Avg");
		CategoryPlot plot = barChart.getCategoryPlot();
		plot.addRangeMarker(marker);
		
		int width = 640;    /* Width of the image */
	    int height = 480;   /* Height of the image */ 
	    File BarChart = new File( "TotalExpensesForNMonthsBackForOnePaymentMethod.jpeg" ); 
	    ChartUtilities.saveChartAsJPEG( BarChart , barChart , width , height );
		
	}
	
	public static void VisualizeTotalExpensesForNMonthsBackForOnePaymentMethodRelativeToAvg(DBManager manager, int userId, int paymentMethod, int monthsback) throws IOException, ClassNotFoundException, SQLException{
		
		ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> listexp = manager.GetAllExpensesByNMonthsForOnePaymentMethod(userId, paymentMethod, monthsback);
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		Double avg = manager.GetAvgAmountFromDBForOnePaymentMethod(userId, paymentMethod, monthsback);
		
		for(int i = 0; i < listexp.size(); i++){
			TotalExpensesForOneMonthForOnePaymentmethod exp = listexp.get(i);
			dataset.addValue(exp.getSumAmount() - avg, exp.getPaymentmethod(), exp.getMonth());		
		}
		
		JFreeChart barChart = ChartFactory.createBarChart("relative Expenses per Month per Paymentmethod", "Expenses per Month per Paymentmethods relative to avg", "Expenses relative to AVG [�]", dataset,PlotOrientation.VERTICAL, true, true, false);
				
		//For Adding the line
		Double position = manager.GetAvgAmountFromDBForOnePaymentMethod(userId, paymentMethod, monthsback);
		
		ValueMarker marker = new ValueMarker(0);
		marker.setPaint(Color.yellow);
		marker.setLabel("Avg");
		CategoryPlot plot = barChart.getCategoryPlot();
		plot.addRangeMarker(marker);
		
		int width = 640;    /* Width of the image */
	    int height = 480;   /* Height of the image */ 
	    File BarChart = new File( "TotalExpensesForNMonthsBackForOnePaymentMethodRelativeToAvg.jpeg" ); 
	    ChartUtilities.saveChartAsJPEG( BarChart , barChart , width , height );
		
	}
	
	public static void VisualizeTotalExpensesForNMonthsBackForEachPaymentMethod(DBManager manager, int userId, int monthsback) throws IOException, ClassNotFoundException, SQLException{
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		ArrayList<PaymentMethod> pms = manager.GetPaymentMethods();
		
		for(int i = 0; i < pms.size(); i++){
			
			ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> listexp = manager.GetAllExpensesByNMonthsForOnePaymentMethod(userId, pms.get(i).getPaymentmethodId(), monthsback);
			
			for(int n = 0; n < listexp.size(); n++){
				TotalExpensesForOneMonthForOnePaymentmethod exp = listexp.get(n);
				dataset.addValue(exp.getSumAmount(), exp.getPaymentmethod(), exp.getMonth());		
			}
		}
			

		JFreeChart barChart = ChartFactory.createBarChart("Expenses per Paymentmethod", "Expenses per Month per Paymentmethod", "Expenses[�]", dataset ,PlotOrientation.VERTICAL, true, true, false);
		
		

		ArrayList<avgExpensesForOnePaymentMethod> list = manager.getAvgExpensesForAllPaymentTypesForNMonths(userId, monthsback);

		
		for(int i = 0; i < pms.size(); i++){

			for(int n = 0; n < list.size(); n++){
				if(list.get(n).getPaymentmethod() == pms.get(i).getPaymentmethodId()){
					ValueMarker marker = new ValueMarker(list.get(n).getAvgExpense());
					marker.setPaint(Color.yellow);
					marker.setLabel("Avg" + manager.GetPaymentMethodWithPaymentMethodId(list.get(n).getPaymentmethod()));
					CategoryPlot plot = barChart.getCategoryPlot();
					plot.addRangeMarker(marker);
				}
				
			}
				
				
			}

		
		int width = 640;    /* Width of the image */
	    int height = 480;   /* Height of the image */ 
	    File BarChart = new File( "TotalExpensesForNMonthsBackForAllPaymentMethods.jpeg" ); 
	    ChartUtilities.saveChartAsJPEG( BarChart , barChart , width , height );
		
	}
	
	public static void VisualizeTotalExpensesForNMonthsBackForEachPaymentMethodRelativeToAVG(DBManager manager, int userId, int monthsback) throws IOException, ClassNotFoundException, SQLException{
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		ArrayList<PaymentMethod> pms = manager.GetPaymentMethods();
		
		ArrayList<avgExpensesForOnePaymentMethod> avgexp = manager.getAvgExpensesForAllPaymentTypesForNMonths(1, 12);
		
		for(int i = 0; i < pms.size(); i++){
			
			ArrayList<TotalExpensesForOneMonthForOnePaymentmethod> listexp = manager.GetAllExpensesByNMonthsForOnePaymentMethod(userId, pms.get(i).getPaymentmethodId(), monthsback);
			
			for(int n = 0; n < listexp.size(); n++){
				TotalExpensesForOneMonthForOnePaymentmethod exp = listexp.get(n);
				dataset.addValue(exp.getSumAmount() - avgexp.get(i).getAvgExpense(), exp.getPaymentmethod(), exp.getMonth());		
			}
		}
			

		JFreeChart barChart = ChartFactory.createBarChart("Expenses per Paymentmethod", "Expenses per Month per Paymentmethod relative to avg Expenses per PM", "Expenses[�]", dataset ,PlotOrientation.VERTICAL, true, true, false);
		
		ValueMarker marker = new ValueMarker(0);
		marker.setPaint(Color.yellow);
		marker.setLabel("Avg Expense");
		CategoryPlot plot = barChart.getCategoryPlot();
		plot.addRangeMarker(marker);
		
		int width = 640;    /* Width of the image */
	    int height = 480;   /* Height of the image */ 
	    File BarChart = new File( "TotalExpensesForNMonthsBackForAllPaymentMethodsRelativeToAvg.jpeg" ); 
	    ChartUtilities.saveChartAsJPEG( BarChart , barChart , width , height );
		
	}
		
	public static void VisualizeTotalExpensesAndTotalEarningsForNMonthsBack(DBManager manager, int userId, int monthsback) throws IOException, ClassNotFoundException, SQLException{
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		ArrayList<totalErnExpPerMonth> list =  manager.getEarningsAndExpensesForNMonths(userId, monthsback);
		
		for(int n = 0; n < list.size(); n++){
		
			totalErnExpPerMonth ernexp = list.get(n);
			
			if(ernexp.getIsExpense()){
				dataset.addValue(ernexp.getTotalamount(), "Expenses", ernexp.getMonth());
			}else{
				dataset.addValue(ernexp.getTotalamount(), "Earnings", ernexp.getMonth());
			}
			
		}
			
		JFreeChart barChart = ChartFactory.createBarChart("total Expenses / Earnings per Month", "Expenses / Earnings per Month", "Expenses[�]", dataset ,PlotOrientation.VERTICAL, true, true, false);
//		
		Double avgexpense = manager.getAvgEarningOrExpenseForNMonths(userId, monthsback, true);
		Double avgearning = manager.getAvgEarningOrExpenseForNMonths(userId, monthsback, false);
			
		ValueMarker m1 = new ValueMarker(avgexpense);
		m1.setPaint(Color.yellow);
		m1.setLabel("Avg Expense");
		CategoryPlot plot = barChart.getCategoryPlot();
		plot.addRangeMarker(m1);
		
		ValueMarker m2 = new ValueMarker(avgearning);
		m2.setPaint(Color.yellow);
		m2.setLabel("Avg Earning");
		plot.addRangeMarker(m2);
		
		int width = 640;    /* Width of the image */
	    int height = 480;   /* Height of the image */ 
	    File BarChart = new File( "TotalExpensesAndEarningsForNMonthsBack.jpeg" ); 
	    ChartUtilities.saveChartAsJPEG( BarChart , barChart , width , height );
		
	}

	public static void VisualizeTotalExpensesPerTransactionPurposeForNMonthsBack(DBManager manager, int userId, int monthsback){
		
		ArrayList<TotalExpensesPerTransactionpurposePerMonth> listExp = new ArrayList<TotalExpensesPerTransactionpurposePerMonth>();
		try {
			listExp = DBManager.getTotalExpensesForEachTransactionTypeWithinNMonths(userId, monthsback);
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		for(int i = 0; i < listExp.size(); i++){
			dataset.setValue(listExp.get(i).getTransactionpurpose(), listExp.get(i).getTotalAmount());
		}
		
		String title = "total Expenses for " + monthsback + " months back sorted by Expensetype";
		
		 JFreeChart chart = ChartFactory.createPieChart(
		         title,   // chart title
		         dataset,          // data
		         true,             // include legend
		         true,
		         false);
		
	      int width = 640;   /* Width of the image */
	      int height = 480;  /* Height of the image */ 
	      File pieChart = new File( "totExpensesForEachTransactionTypeForNMonthsBack.jpeg" ); 
	      try {
			ChartUtilities.saveChartAsJPEG( pieChart , chart , width , height );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}


