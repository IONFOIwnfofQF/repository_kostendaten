package Models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ReadFile {
	
	FileReader fr;
	BufferedReader br;
	
	public ReadFile(){}
	
//	public ArrayList<EarningExpense> readCSV(String fpath) throws IOException, ParseException{
//		fr = new FileReader(fpath);
//		br = new BufferedReader(fr);
//
//		ZoneId zoneId = ZoneId.systemDefault(); 
//		
//		ArrayList<EarningExpense> lines = new ArrayList<EarningExpense>();
//		String line = null;
//		
//		//Is executed once to eliminate the first line with the line designation
//		line = br.readLine();	
//		line = br.readLine();	
//		
//		while(line != null){
//			System.out.println(line);
//			EarningExpense earexp = new EarningExpense();
//	
//			String[] lineedit = line.split(";");
//					
//			//Gets the Date of the Expense / Earning  
//			DateTimeFormatter formCSV = DateTimeFormatter.ofPattern("dd.MM.yyyy");
//			earexp.set_date(LocalDate.parse(lineedit[0], formCSV));
//			
//			//Line for the Calender Week, which is unneccassary if you have the Date
//				
//			String expensecash = changeCommaAndRemoveLastDigitOfValue(lineedit[2].trim());
//			earexp.set_expensecash(Double.parseDouble(expensecash));
//			
//			String expensecard = changeCommaAndRemoveLastDigitOfValue(lineedit[3].trim());
//			earexp.set_expensecard(Double.parseDouble(expensecard));		
//			
//			String earningcash = changeCommaAndRemoveLastDigitOfValue(lineedit[4].trim());
//			earexp.set_earningcash(Double.parseDouble(earningcash));
//			
//			String earningcard = changeCommaAndRemoveLastDigitOfValue(lineedit[5].trim());			
//			earexp.set_earningcard(Double.parseDouble(earningcard));
//			
//			String purpose = lineedit[6].trim().toLowerCase();		
//			purpose = RemoveCommaFromString(purpose);
//			earexp.set_purpose(purpose);
//					
//			//Month and Year are saved in the sheet, but aren't neccessary due to
//			//us already having the date		
//				
//			try{
//					String description = lineedit[9].trim().toLowerCase();
//					description = RemoveCommaFromString(description);
//					earexp.set_description(description);	
//
//			}catch(ArrayIndexOutOfBoundsException ex){
//				earexp.set_description("");
//			}
//			
//			lines.add(earexp);
//			
//			line = br.readLine();	
//			
//		}
//				
//		return lines;
//		
//	}
//	
	public ArrayList<Transaction> getAllTransactionsFromCSV(String fpath, ArrayList<TransactionPurpose> transactionpurposes) throws IOException{

		fr = new FileReader(fpath);
		br = new BufferedReader(fr);

		ZoneId zoneId = ZoneId.systemDefault(); 
		
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();

		String line = null;
		
		//Is executed once to eliminate the first line with the line designation
		line = br.readLine();	
		line = br.readLine();
		
		while(line != null){
			
			String[] lineedit = line.split(";");
			Transaction t = new Transaction();	

			DateTimeFormatter formCSV = DateTimeFormatter.ofPattern("dd.MM.yyyy");
			t.setDate(LocalDate.parse(lineedit[0], formCSV));
			
			// I'm the first User with the USerID 1
			t.setUser(1);
			
			try{
				t.setDescription(lineedit[9].trim());
			}catch(java.lang.ArrayIndexOutOfBoundsException ex){
				t.setDescription("");
			}
			
			//Expenses cash 
			if(lineedit[2].isEmpty() == false){
				
				String amount = changeCommaAndRemoveLastDigitOfValue(lineedit[2].trim());
				t.setAmount(Double.parseDouble(amount));
				//Paymentmethod 1 is cash
				t.setPaymentmethod(1);
				t.setIsexpense(true);
				t.setTransactionpurpose(GetPositionForTransactionPurpose(transactionpurposes, lineedit[6].trim().toLowerCase()));
							
			}
			
			//Expenses debit
			if(lineedit[3].isEmpty() == false){
				
				String amount = changeCommaAndRemoveLastDigitOfValue(lineedit[3].trim());
				t.setAmount(Double.parseDouble(amount));
				//Paymentmethod 2 is Debitcard
				t.setPaymentmethod(2);
				t.setIsexpense(true);
				t.setTransactionpurpose(GetPositionForTransactionPurpose(transactionpurposes, lineedit[6].trim().toLowerCase()));
			}
			
			//Income cash
			if(lineedit[4].isEmpty() == false){
				
				String amount = changeCommaAndRemoveLastDigitOfValue(lineedit[4].trim());
				t.setAmount(Double.parseDouble(amount));
				t.setPaymentmethod(1);
				t.setIsexpense(false);
				t.setTransactionpurpose(GetPositionForTransactionPurpose(transactionpurposes, lineedit[6].trim().toLowerCase()));
							
			}
			
			//Income debit
			if(lineedit[5].isEmpty() == false){
				
				String amount = changeCommaAndRemoveLastDigitOfValue(lineedit[5].trim());
				t.setAmount(Double.parseDouble(amount));
				t.setPaymentmethod(2);
				t.setIsexpense(false);
				t.setTransactionpurpose(GetPositionForTransactionPurpose(transactionpurposes, lineedit[6].trim().toLowerCase()));
							
			}
			
			transactions.add(t);
			line = br.readLine();
			
		}
		
		return transactions;
		
	}
	
	public ArrayList<TransactionPurpose> getAllTransactionPurposesFromCSV(String fpath) throws IOException{
		
		fr = new FileReader(fpath);
		br = new BufferedReader(fr);
		
		ArrayList<TransactionPurpose> transactiontypes = new ArrayList<TransactionPurpose>();
		String line = null;
		
		//Is executed once to eliminate the first line with the line designation
		line = br.readLine();	
		line = br.readLine();	
		
		while(line != null){		
			
			TransactionPurpose transactiontype = new TransactionPurpose();
			transactiontype.setUser(1);
						
			String[] lineedit = line.split(";");
			
			System.out.println(line.toString());
			
			// Checks, if the line is a Expense or Income	
			
			if(lineedit[4].isEmpty() && lineedit[5].isEmpty()){
				transactiontype.setIsexpense(true);			
			}
			if(lineedit[2].isEmpty() && lineedit[3].isEmpty()){
				transactiontype.setIsexpense(false);			
			}
			

			//Checks the list with all of the expensetypes, if the expense already exists	
			
			String tpurpose = lineedit[6].trim().toLowerCase();
			tpurpose = RemoveCommaFromString(tpurpose);
			boolean alreadyContainsTransactiontype = false;
				
				if(transactiontypes.size() != 0){	
					
					for(int i = 0; i < transactiontypes.size(); i++){
						if( transactiontypes.get(i).getTransactionpurpose().equals(tpurpose)){
							alreadyContainsTransactiontype = true;
						}
					}
						
					if(alreadyContainsTransactiontype == false){
						transactiontype.setTransactionpurpose(tpurpose);
						
						transactiontypes.add(transactiontype);
					}
				}			
				else{				
						String tp = lineedit[6].trim().toLowerCase();
						tp = RemoveCommaFromString(tp);				
						transactiontype.setTransactionpurpose(tp);
						
						transactiontypes.add(transactiontype);
						
					}
				line = br.readLine();
				}
					

				return transactiontypes;	
			}

	public static String changeCommaAndRemoveLastDigitOfValue(String value){

		if(value.isEmpty() || value.length() == 0 || value == null){
			return "0.0";
		}
		//Goes until the "," of a field and replaces it with ".", this is necessary, because the MySql Database recognises "." 
		//as a comma while excel defines "," as the comma
		//after that, the value goes from the comma to the end -1 digit, so that the "�" digit can be removed
		
		value = value.trim();
		
		value = value.substring(0, value.indexOf(",")) +"."+ value.substring(value.indexOf(",") + 1,value.length()-1);	
		
		//Removes all of the whitespaces between the numbers (1 000 => 1000)
		value = value.replaceAll("\\s+","");
		return value;
	}
	
	public static String RemoveCommaFromString(String v){
		if(v.isEmpty() || v.length() == 0 || v == null){
			return "";
		}
		if(!v.contains("'")){
			return v;
		}
		v = v.substring(0, v.indexOf("'") ) + v.substring(v.indexOf("'")+1, v.length());
		
		return v;
	}
	
	public int GetPositionForTransactionPurpose(ArrayList<TransactionPurpose> transactionpurposes, String tp){
		
		for(int i = 0; i < transactionpurposes.size(); i++){
			
			//System.out.println("nr:" + i + " "+expensetypes.get(i).getExpensetype());
			
			if( transactionpurposes.get(i).getTransactionpurpose().equals(tp.trim().toLowerCase())){
				return i+1;
			}
		}
		return 0;	
}

}
