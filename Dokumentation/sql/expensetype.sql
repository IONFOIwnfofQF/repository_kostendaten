CREATE TABLE IF NOT EXISTS `expensetype` (
  `expensetypeId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `expensetype` varchar(20) NOT NULL,
  PRIMARY KEY (`expensetypeId`),
  FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
)