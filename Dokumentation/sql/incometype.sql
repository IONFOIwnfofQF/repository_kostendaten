CREATE TABLE IF NOT EXISTS `incometype` (
  `incometypeId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `incometype` varchar(20) NOT NULL,
  PRIMARY KEY (`incometypeId`),
  FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
)
