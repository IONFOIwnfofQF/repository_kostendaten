CREATE TABLE `transactionpurpose` (
  `transactionpurposeId` int(11) NOT NULL AUTO_INCREMENT,
  `isexpense` tinyint(1) NOT NULL,
  `transactionpurpose` varchar(45) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`transactionpurposeId`),
  FOREIGN KEY (`user`) REFERENCES `user` (`userId`)
) 