CREATE TABLE IF NOT EXISTS `transaction` (
  `transactionId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(11,2) NOT NULL,
  `isexpense` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `user` int(11) NOT NULL,
  `paymentmethod` int(11) NOT NULL,
  `transactionpurpose` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`transactionId`),
  FOREIGN KEY (`paymentmethod`) REFERENCES `paymentmethod` (`paymentmethodId`),
  FOREIGN KEY (`transactionpurpose`) REFERENCES `transactionpurpose` (`transactionpurposeId`),
  FOREIGN KEY (`user`) REFERENCES `user` (`userId`)
) 
